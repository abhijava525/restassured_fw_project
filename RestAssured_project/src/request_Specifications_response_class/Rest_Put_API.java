package request_Specifications_response_class;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_Put_API {

	public static void main(String[] args) {
		
		//Step 1 : Collect all needed information and save it into local variables

		String req_body = "{\r\n"+ "    \"name\": \"morpheus\",\r\n"+ "    \"job\": \"zion resident\"\r\n"+ "}";
			
			String hostname = "https://reqres.in/";
			
			String resource = "api/users/2";
			
			String headername = "Content-Type";
			
			String headervalue = "application/json";
			
			
			//step2 : Build the RequestSpecification by using RequestSpecification class
			
			RequestSpecification req_spec = RestAssured.given();
			
			//step 2.1: set the Request header
			
			req_spec.header(headername,headervalue);
			
			//step2.2: set the Request body
			
			req_spec.body(req_body);
			
		//step3 :Trigger the API request 
			
			Response response = req_spec.put(hostname + resource);
			
			System.out.println(response.getBody().asString());
			System.out.println(response.statusCode());
			
			// Step 4 : Parse the response body

			ResponseBody res_body = response.getBody();

			String res_name = res_body.jsonPath().getString("name");
			System.out.println(res_name);
			String res_job = res_body.jsonPath().getString("job");
			System.out.println(res_job);
			String res_updatedAt = res_body.jsonPath().getString("updatedAt");
			res_updatedAt = res_updatedAt.substring(0, 11);
			System.out.println(res_updatedAt);

			// Step 5 : Validate the response body

			// Step 5.1 : Parse request body and save into local variables

			JsonPath jsp_req = new JsonPath(req_body);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");

			// Step 5.2 : Generate expected date

			LocalDateTime currentdate = LocalDateTime.now();
			String expecteddate = currentdate.toString().substring(0, 11);

			// Step 5.3 : Use TestNG's Assert

			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
			Assert.assertEquals(res_updatedAt, expecteddate);

			
		
	}

}
