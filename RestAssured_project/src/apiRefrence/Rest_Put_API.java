package apiRefrence;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Rest_Put_API {

	public static void main(String[] args) {
		//Step 1 : Collect all needed information and save it into local variables

	String req_body = "{\r\n"+ "    \"name\": \"morpheus\",\r\n"+ "    \"job\": \"zion resident\"\r\n"+ "}";
		
		String hostname = "https://reqres.in/";
		
		String resource = "api/users/2";
		
		String headername = "Content-Type";
		
		String headervalue = "application/json";
		
		// Step 2 : Declare BaseURI 
		
				RestAssured.baseURI =hostname;
				
	    // Step 3 : Configure the API for execution and save the response in a String variable
				String res_body = given().header(headername,headervalue).body(req_body).when().put(resource).then().extract().response().asString();
			    
				System.out.println(res_body);
				
				// Step 4.1 : Create the object of JsonPath
				
				JsonPath jsp_res = new JsonPath(res_body);
				
				//4.2: Parse individual params using jsp_res object
				//1.name
				String res_name = jsp_res.getString("name");
				
				System.out.println(res_name);
				
				//2.job
				
				String res_job = jsp_res.getString("job");
				System.out.println(res_job);
				
				//3.
				String res_updatedAt = jsp_res.getString("updatedAt");
				res_updatedAt = res_updatedAt.substring(0, 11);
				System.out.println(res_updatedAt);
				
				// Step 5 : Validate the response body
				
				// Step 5.1 : Parse request body and save into local variables
		
		JsonPath jsp_req = new JsonPath(req_body);
		
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		// Step 5.2 : Generate expected date
		
				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0,11);
				
	// Step 5.3 : Use TestNG's Assert for validation
				
				Assert.assertEquals(res_name, req_name);
				Assert.assertEquals(res_job, req_job);
				
				Assert.assertEquals(res_updatedAt, expecteddate);
				
	}

}
