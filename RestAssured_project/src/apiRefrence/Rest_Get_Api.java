package apiRefrence ;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import io.restassured.response.Response;

import org.testng.Assert;

public class Rest_Get_Api {

	public static void main(String[] args) {

		// Step 1 : Collect all needed information and save it into local variables

		String hostname = "https://reqres.in/";

		String resource = "/api/unkown";
		
		// Step 2 : Declare BaseURI 
		
		RestAssured.baseURI=hostname;
		// Step 3 : Configre the API for execution and log entire transaction (request header , request body , response header , response body , time etc)
		/*
		 * given().header(headername, headervalue).body(req_body).log().all().post(resource).then().log().all().extract().
		response();
		// Make a GET request and store the response
		*/
		
        Response res_body = given().when().get(resource );
	
        // Extract the status code and the body
        int status = res_body.getStatusCode();
        String body = res_body.getBody().asString();
        
        // Print the status and the body to the console
        System.out.println( status);
        System.out.println(body);
		
		
		// Step 5 : Parse the response body
		
		// Step 5.1 : Create the object of JsonPath
		JsonPath jsp_res = new JsonPath(body);

        System.out.println( jsp_res);
		
		// Step 5.2 : Parse individual params using jsp_res object 
        String res_id=jsp_res.getString("data.id");
		System.out.println(res_id);
		String res_name=jsp_res.getString("data.name");
		System.out.println(res_name);
		String res_year=jsp_res.getString("data.year");
		System.out.println(res_year);
		String res_color=jsp_res.getString("data.color");
		System.out.println(res_color);
		String res_5value=jsp_res.getString("data.pantone_value");
		System.out.println(res_5value);
		
		//step  6.0 store expected result
		String exp_id="[1, 2, 3, 4, 5, 6]";
		String exp_name="[cerulean, fuchsia rose, true red, aqua sky, tigerlily, blue turquoise]";
		String exp_year="[2000, 2001, 2002, 2003, 2004, 2005]";
		String exp_color="[#98B2D1, #C74375, #BF1932, #7BC4C4, #E2583E, #53B0AE]";
		String exp_5value="[15-4020, 17-2031, 19-1664, 14-4811, 17-1456, 15-5217]";
		
		// Step 6 : Validate the response body

		// Step 6.2 : Use TestNG's Assert
		Assert.assertEquals(status,200 );      
		Assert.assertEquals(res_id, exp_id);
		Assert.assertEquals(res_name, exp_name);
		Assert.assertEquals(res_year, exp_year);
		Assert.assertEquals(res_color, exp_color);
		Assert.assertEquals(res_5value, exp_5value);
	}

}

