package Runner;

import java.io.IOException;

import Common_Methods.Utility;
import TestPackage.Create_User;
import TestPackage.Update_User;
import TestPackage.Update_User_Put;
import TestPackage.Get_user_Data;
import TestPackage.Delete_Data;

public class Common_Runner {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Create_User.Executor();
		Update_User.Executor();
		Update_User_Put.Executor();
		Get_user_Data.Executor();
		Delete_Data.Executor();
	}

}
