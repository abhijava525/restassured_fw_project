Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-19T15:39:17.023Z"}Response Header is : 
Tue, 19 Mar 2024 15:39:17 GMT

